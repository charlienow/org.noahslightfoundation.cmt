{* FILE: cmt/templates/messageTemplateSelector.tpl *}
{* template block that contains the new field *}
<div id="mt-tr">
    <div style="display: inline-block;width:150px;text-align: right;padding: 4px 4px 4px 6px;border: 0 none;vertical-align: top;">Message Template</div>
    <div style="display:inline-block">{$form.messageTemplates.html}
        <p>Please note the selected message template will also be used for contribution receipts to Personal Campaigns.</p>
    </div>

</div>

{* reposition the above block *}
{if !empty($jsPlacement)}
<script type="text/javascript">
    {literal}
    CRM.$(function($) {
        $("#mt-tr").insertAfter($("{/literal}{$jsPlacement}{literal}").first());
    });
    {/literal}
</script>
{/if}