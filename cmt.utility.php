<?php

/**
 * The oUtility class is an interface
 * to CiviCRM
 */
class CRM_Cmt_oUtility {

    public static function getUserTemplates() {
        // get user-made available message templates
        try {
            $allTemplates = civicrm_api3('MessageTemplate', 'get', array(
                'return' => array("msg_title"),
                'workflow_id' => array('IS NULL' => 1),
                'is_active' => 1,
            ));

            if ($allTemplates['count'] < 1) return FALSE;
            return $allTemplates['values'];
        } catch (\Exception $e) {
            throw new Exception($e->__toString());
            return FALSE;
        }
    }

}