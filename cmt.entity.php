<?php

/**
 * The oEntity class
 */
class CRM_Cmt_oEntity {

    public $entity_name;
    public $prefix;
    public $forms; // array
    public $form; // the form we're dealing with

    public function __construct($entity) {
        $this->entity_name = $entity['name'];
        $this->prefix = $entity['prefix'];
        $this->forms = $entity['forms'];
    }

    public function getName() {
        return $this->entity_name;
    }

    public function getPrefix() {
        return $this->prefix;
    }

    public function setForm($formName) {
        $this->form = $formName;
    }

    public function getJsLocation() {
        foreach ($this->forms as $form => $jsPlacement) {
            if (empty($jsPlacement)) return NULL;
            if ($this->form == $form) return $jsPlacement;
        }
        return NULL;
    }

}